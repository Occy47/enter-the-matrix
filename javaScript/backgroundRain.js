﻿var canvas = document.createElement("canvas");
var c = canvas.getContext("2d");

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

window.onload = function () {
    document.body.appendChild(canvas);
};

//test varijabla
var test = Math.round(Math.random() * 10) + 2;


var symbolSize = "40px katakana";

var streams = [];

function CreateStreams() {
    var x = 0;
    var y = 0;
    for (var i = 0; i <= canvas.width / 40; i++) {
        var stream = new Stream();
        stream.createSymbols(x, y);
        streams.push(stream);
        x += 40;

    }

}
function ClearCanvas() {
    c.fillStyle = "black";
    c.fillRect(0, 0, canvas.width, canvas.height);
}

function MatrixSymbol(x, y, speed) {
    this.x = x;
    this.y = y;
    this.value;
    this.speed = speed;

    this.randomizeSymbol = function () {
        this.value = String.fromCharCode(
            0x30A0 + Math.random() * (0x30FF - 0x30A0 + 1)
        );
    };

    this.rainEffect = function () {
        if (this.y > canvas.height) {
            this.y = 0;
        }
        else {
            this.y += this.speed;
        }

        console.log("test rain");
    };
}

function Stream() {
    this.symbols = [];
    this.numberOfSymbols = Math.round(Math.random() * 25) + 4;
    this.speed = Math.round(Math.random() * 45) + 4;

    this.createSymbols = function (x,y) {
        for (i = 0; i < this.numberOfSymbols; i++) {
           var symbol = new MatrixSymbol(x, y, this.speed);
            symbol.randomizeSymbol();
            this.symbols.push(symbol);
            y -= 40;
        }
    };

    this.renderSymbols = function () {
        this.symbols.forEach(function (symbol) {
            c.font = symbolSize;
            c.fillStyle = "#00FF00";
            c.fillText(symbol.value, symbol.x, symbol.y);
            symbol.rainEffect();
            symbol.randomizeSymbol();           
        });
    };
}

function Draw() {
    ClearCanvas();

    streams.forEach(function (stream) {
        stream.renderSymbols();
    });
    console.log("res");
}


CreateStreams();
console.log(streams);

setInterval(function () { Draw(); }, 130);


